# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2014, 2015, 2016, 2017, 2018, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-01-23 02:49+0100\n"
"PO-Revision-Date: 2020-06-15 10:51+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.04.1\n"

#: contents/ui/ConnectionItem.qml:61
#, kde-format
msgid "Connect"
msgstr "Verbinden"

#: contents/ui/ConnectionItem.qml:61
#, kde-format
msgid "Disconnect"
msgstr "Verbinding verbreken"

#: contents/ui/ConnectionItem.qml:97
#, kde-format
msgid "Show network's QR code"
msgstr "QR code van netwerk tonen"

#: contents/ui/ConnectionItem.qml:108
#, kde-format
msgid "Configure..."
msgstr "Configureren..."

#: contents/ui/ConnectionItem.qml:132
#, kde-format
msgid "Speed"
msgstr "Snelheid"

#: contents/ui/ConnectionItem.qml:137
#, kde-format
msgid "Details"
msgstr "Details"

#: contents/ui/ConnectionItem.qml:275
#, kde-format
msgid "Connected, ⬇ %1/s, ⬆ %2/s"
msgstr "Verbonden, ⬇ %1/s, ⬆ %2/s"

#: contents/ui/ConnectionItem.qml:279
#, kde-format
msgid "Connected"
msgstr "Verbonden"

#: contents/ui/DetailsText.qml:44
#, kde-format
msgid "Copy"
msgstr "Kopiëren"

#: contents/ui/main.qml:33
#, kde-format
msgid "Networks"
msgstr "Netwerken"

#: contents/ui/main.qml:57
#, kde-format
msgid "&Configure Network Connections..."
msgstr "&Netwerkverbindingen instellen..."

#: contents/ui/main.qml:60
#, kde-format
msgid "Open Network Login Page..."
msgstr "Netwerkaanmeldpagina openen..."

#: contents/ui/PasswordField.qml:30
#, kde-format
msgid "Password..."
msgstr "Wachtwoord..."

#: contents/ui/PopupDialog.qml:74
#, kde-format
msgid "Airplane mode is enabled"
msgstr "Vliegtuigmodus ingeschakeld"

#: contents/ui/PopupDialog.qml:78
#, kde-format
msgid "Wireless and mobile networks are deactivated"
msgstr "Draadloze en mobiele netwerken zijn gedeactiveerd"

#: contents/ui/PopupDialog.qml:80
#, kde-format
msgid "Wireless is deactivated"
msgstr "Draadloos is gedeactiveerd"

#: contents/ui/PopupDialog.qml:83
#, kde-format
msgid "Mobile network is deactivated"
msgstr "Mobiel netwerk is gedeactiveerd"

#: contents/ui/PopupDialog.qml:85
#, kde-format
msgid "No available connections"
msgstr "Geen beschikbare verbindingen"

#: contents/ui/Toolbar.qml:80
#, kde-format
msgid "Enable Wi-Fi"
msgstr "Wi-Fi inschakelen"

#: contents/ui/Toolbar.qml:96
#, kde-format
msgid "Enable mobile network"
msgstr "Mobiele netwerk inschakelen"

#: contents/ui/Toolbar.qml:131
#, kde-kuit-format
msgctxt "@info"
msgid "Disable airplane mode<nl/><nl/>This will enable Wi-Fi and Bluetooth"
msgstr ""
"Vliegtuigmodus uitschakelen<nl/><nl/>Dit zal Wi-Fi en Bluetooth inschakelen"

#: contents/ui/Toolbar.qml:132
#, kde-kuit-format
msgctxt "@info"
msgid "Enable airplane mode<nl/><nl/>This will disable Wi-Fi and Bluetooth"
msgstr ""
"Vliegtuigmodus inschakelen<nl/><nl/>Dit zal Wi-Fi en Bluetooth uitschakelen"

#: contents/ui/Toolbar.qml:142
#, kde-format
msgid "Hotspot"
msgstr "Hotspot"

#: contents/ui/Toolbar.qml:163 contents/ui/Toolbar.qml:174
#, kde-format
msgid "Disable Hotspot"
msgstr "Hotspot uitschakelen"

#: contents/ui/Toolbar.qml:168 contents/ui/Toolbar.qml:174
#, kde-format
msgid "Create Hotspot"
msgstr "Hotspot aanmaken"

#: contents/ui/Toolbar.qml:185
#, kde-format
msgctxt "text field placeholder text"
msgid "Search..."
msgstr "Zoeken..."

#: contents/ui/Toolbar.qml:200
#, kde-format
msgid "Configure network connections..."
msgstr "Netwerkverbindingen instellen..."

#: contents/ui/TrafficMonitor.qml:50 contents/ui/TrafficMonitor.qml:114
#, kde-format
msgid "/s"
msgstr "/s"

#: contents/ui/TrafficMonitor.qml:96
#, kde-format
msgid "Upload"
msgstr "Uploaden"

#: contents/ui/TrafficMonitor.qml:96
#, kde-format
msgid "Download"
msgstr "Downloaden"