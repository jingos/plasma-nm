# translation of plasmanetworkmanagement_openconnectui.po to Slovak
# Roman Paholík <wizzardsk@gmail.com>, 2014, 2015, 2017, 2019.
# Matej Mrenica <matejm98mthw@gmail.com>, 2019, 2020.
# Dusan Kazik <prescott66@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasmanetworkmanagement_openconnectui\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-01-21 02:51+0100\n"
"PO-Revision-Date: 2020-11-27 14:28+0100\n"
"Last-Translator: Matej Mrenica <matejm98mthw@gmail.com>\n"
"Language-Team: Slovak <kde-i18n-doc@kde.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.11.80\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: openconnectauth.cpp:310
#, kde-format
msgid "Failed to initialize software token: %1"
msgstr "Zlyhala inicializácia softvérového tokenu: %1"

#: openconnectauth.cpp:363
#, kde-format
msgid "Contacting host, please wait..."
msgstr "Kontaktuje sa hostiteľ. Prosím čakajte..."

#: openconnectauth.cpp:589
#, kde-format
msgctxt "Verb, to proceed with login"
msgid "Login"
msgstr "Prihlásiť"

#: openconnectauth.cpp:647
#, kde-format
msgid ""
"Check failed for certificate from VPN server \"%1\".\n"
"Reason: %2\n"
"Accept it anyway?"
msgstr ""
"Zlyhala kontrola certifikátu zo servera VPN \"%1\".\n"
"Dôvod: %2\n"
"Prijať ho aj napriek tomu?"

#: openconnectauth.cpp:746
#, kde-format
msgid "Connection attempt was unsuccessful."
msgstr "Pokus o pripojenie bol neúspešný."

#. i18n: ectx: property (windowTitle), widget (QWidget, OpenconnectAuth)
#: openconnectauth.ui:26
#, kde-format
msgid "OpenConnect VPN Authentication"
msgstr "Overenie totožnosti OpenConnect VPN"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: openconnectauth.ui:55
#, kde-format
msgid "VPN Host"
msgstr "Hostiteľ VPN"

#. i18n: ectx: property (toolTip), widget (QPushButton, btnConnect)
#: openconnectauth.ui:81
#, kde-format
msgid "Connect"
msgstr "Pripojiť"

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoconnect)
#: openconnectauth.ui:102
#, kde-format
msgid "Automatically start connecting next time"
msgstr "Nabudúce automaticky spustiť pripojenie"

#. i18n: ectx: property (text), widget (QCheckBox, chkStorePasswords)
#: openconnectauth.ui:109
#, kde-format
msgid "Store passwords"
msgstr "Uložiť heslá"

#. i18n: ectx: property (text), widget (QCheckBox, viewServerLog)
#: openconnectauth.ui:153
#, kde-format
msgid "View Log"
msgstr "Zobraziť záznam"

#. i18n: ectx: property (text), widget (QLabel, lblLogLevel)
#: openconnectauth.ui:163
#, kde-format
msgid "Log Level:"
msgstr "Úroveň záznamu:"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbLogLevel)
#: openconnectauth.ui:174
#, kde-format
msgid "Error"
msgstr "Chyba"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbLogLevel)
#: openconnectauth.ui:179
#, kde-format
msgid "Info"
msgstr "Informácie"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbLogLevel)
#: openconnectauth.ui:184
#, kde-format
msgctxt "like in Debug log level"
msgid "Debug"
msgstr "Ladenie"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbLogLevel)
#: openconnectauth.ui:189
#, kde-format
msgid "Trace"
msgstr "Trasovanie"

#. i18n: ectx: property (windowTitle), widget (QWidget, OpenconnectProp)
#: openconnectprop.ui:20
#, kde-format
msgid "OpenConnect Settings"
msgstr "Nastavenia OpenConnect"

#. i18n: ectx: property (title), widget (QGroupBox, grp_general)
#: openconnectprop.ui:26
#, kde-format
msgctxt "like in General settings"
msgid "General"
msgstr "Všeobecné"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: openconnectprop.ui:38
#, kde-format
msgid "Gateway:"
msgstr "Brána:"

#. i18n: ectx: property (text), widget (QLabel, label)
#: openconnectprop.ui:51
#, kde-format
msgid "CA Certificate:"
msgstr "Certifikát CA:"

#. i18n: ectx: property (filter), widget (KUrlRequester, leCaCertificate)
#. i18n: ectx: property (filter), widget (KUrlRequester, leUserCert)
#. i18n: ectx: property (filter), widget (KUrlRequester, leUserPrivateKey)
#: openconnectprop.ui:61 openconnectprop.ui:212 openconnectprop.ui:229
#, kde-format
msgid "*.pem *.crt *.key"
msgstr "*.pem *.crt *.key"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: openconnectprop.ui:68
#, kde-format
msgid "Proxy:"
msgstr "Proxy:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: openconnectprop.ui:81
#, kde-format
msgid "CSD Wrapper Script:"
msgstr "Skript CSD wrappera:"

#. i18n: ectx: property (text), widget (QCheckBox, chkAllowTrojan)
#: openconnectprop.ui:91
#, kde-format
msgid "Allow Cisco Secure Desktop trojan"
msgstr "Umožniť trójana Cisco Secure Desktop"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: openconnectprop.ui:101
#, kde-format
msgid "VPN Protocol:"
msgstr "Protokol siete VPN:"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:118
#, kde-format
msgid "Cisco AnyConnect"
msgstr "Cisco AnyConnect"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:123
#, fuzzy, kde-format
#| msgid "Juniper/Pulse Network Connect"
msgid "Juniper Network Connect"
msgstr "Sieťové pripojenie Juniper/Pulse"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:128
#, kde-format
msgid "PAN Global Protect"
msgstr "Globálna ochrana PAN"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:133
#, kde-format
msgid "Pulse Connect Secure"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: openconnectprop.ui:141
#, kde-format
msgid "Reported OS:"
msgstr "Nahlásený OS:"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:160
#, kde-format
msgid "GNU/Linux"
msgstr "GNU/Linux"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:165
#, kde-format
msgid "GNU/Linux 64-bit"
msgstr "GNU/Linux 64-bitový"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:170
#, kde-format
msgid "Windows"
msgstr "Windows"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:175
#, kde-format
msgid "Mac OS X"
msgstr "Mac OS X"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:180
#, kde-format
msgid "Android"
msgstr "Android"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:185
#, kde-format
msgid "Apple iOS"
msgstr "Apple iOS"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: openconnectprop.ui:196
#, kde-format
msgid "Certificate Authentication"
msgstr "Overenie totožnosti certifikátu"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: openconnectprop.ui:202
#, kde-format
msgid "User Certificate:"
msgstr "Certifikát používateľa:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: openconnectprop.ui:219
#, kde-format
msgid "Private Key:"
msgstr "Súkromný kľúč:"

#. i18n: ectx: property (text), widget (QCheckBox, chkUseFsid)
#: openconnectprop.ui:236
#, kde-format
msgid "Use FSID for key passphrase"
msgstr "Použiť FSID pre heslo kľúča"

#. i18n: ectx: property (text), widget (QCheckBox, preventInvalidCert)
#: openconnectprop.ui:243
#, kde-format
msgid "Prevent user from manually accepting invalid certificates"
msgstr "Zabrániť používateľovi v ručnom prijímaní neplatných certifikátov"

#. i18n: ectx: property (text), widget (QPushButton, buTokens)
#: openconnectprop.ui:269
#, kde-format
msgid "Token Authentication"
msgstr "Overenie totožnosti tokenu"

#. i18n: ectx: property (windowTitle), widget (QWidget, OpenConnectToken)
#: openconnecttoken.ui:14
#, kde-format
msgid "OpenConnect OTP Tokens"
msgstr "Tokeny OpenConnect OTP"

#. i18n: ectx: property (title), widget (QGroupBox, gbToken)
#: openconnecttoken.ui:20
#, kde-format
msgid "Software Token Authentication"
msgstr "Overenie totožnosti softvérového tokenu"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: openconnecttoken.ui:26
#, kde-format
msgid "Token Mode:"
msgstr "Režim tokenu:"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: openconnecttoken.ui:43
#, kde-format
msgid "Token Secret:"
msgstr "Tajomstvo tokenu:"